package com.example.todotbouche;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.SparseBooleanArray;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    public final static int INTENT_CODE = 3600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        load();

       FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                demandeAjout(view);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.effacerSelection :
                deleteItems();
                break;

            case R.id.sauvegarderListe :
                save();
                break;
        }

        return true;
    }

    public void demandeAjout(View monBoutton){
        Intent intent = new Intent(this, SecondActivity.class);
        startActivityForResult(intent,INTENT_CODE);
    }

    public void deleteItems(){
        ListView listView = findViewById(R.id.myList);
        SparseBooleanArray checked = listView.getCheckedItemPositions();
        ArrayAdapter adapter = (ArrayAdapter) listView.getAdapter();

        for(int i = adapter.getCount() - 1; i >= 0; --i){
            if(checked.get(i)){
                adapter.remove(adapter.getItem(i));
            }
        }
        listView.clearChoices();
    }

    public void save(){
        String items = "";
        ListView listView = findViewById(R.id.myList);
        Adapter adapter = listView.getAdapter();
        for (int i = 0; i < adapter.getCount(); ++i){
            items += adapter.getItem(i) + "\n";
        }
        writeToFile(items);
    }

    public void load(){
        String items = readFile();
        ListView myList = findViewById(R.id.myList);
        ArrayList<String> entrees = new ArrayList<>();

        if(!items.equals("")) {
            String[] itemList = items.split("\n");
            for (int i = 0; i < itemList.length; ++i) {
                entrees.add(itemList[i]);
            }
        }

        ArrayAdapter myAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_checked, entrees);
        myList.setAdapter(myAdapter);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode != INTENT_CODE) {
            return;
        }
        String toAdd = data.getStringExtra(SecondActivity.KEY);
        if(!toAdd.equals("")){
            ListView listView = findViewById(R.id.myList);
            ArrayAdapter arrayAdapter= (ArrayAdapter) listView.getAdapter();
            arrayAdapter.add(toAdd);
        }
    }

    private void writeToFile(String data){
        try{
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.openFileOutput("data.txt",this.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private String readFile(){
        String ret = "";

        try{
            InputStream inputStream = this.openFileInput("data.txt");

            if(inputStream != null){
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receivedString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receivedString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receivedString + "\n");
                }
                ret = stringBuilder.toString();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }
}
