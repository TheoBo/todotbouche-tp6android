package com.example.todotbouche;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    public static final String KEY = "add";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }

    public void confirm(View monBoutton){
        EditText editText = findViewById(R.id.toAdd);
        String toSend = editText.getText().toString();
        Intent answer = new Intent();
        answer.putExtra(KEY, toSend);
        setResult(RESULT_OK, answer);
        finish();
    }

    public void cancel(View monBoutton){
        Intent answer = new Intent();
        answer.putExtra(KEY, "");
        setResult(RESULT_OK, answer);
        finish();
    }
}
